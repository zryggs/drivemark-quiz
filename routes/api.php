<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'Auth\LoginController@loginViaToken');
Route::get('currency', 'API\SoapController@show');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:sanctum']], function() {
    Route::get('my-profile', 'API\UserController@show');

    Route::get('loan', 'API\LoanController@index');
    Route::get('loan/payments/all', 'API\LoanController@getAllLoanPayments');
    Route::get('loan/payments', 'API\LoanController@getLoanPayments');
    Route::post('loan/apply', 'API\LoanController@applyLoan');
    Route::post('loan/pay', 'API\LoanController@payLoan');

    Route::group(['middleware' => 'admin'], function () {
        Route::get('loan/all', 'API\LoanController@allUserLoans');
        Route::post('loan/updateStatus', 'API\LoanController@updateLoanStatus'); // middleware check admin
    });
});
