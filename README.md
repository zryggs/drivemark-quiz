## Installation and Configuration

- composer install. Enable your php soap client.
- npm install
- Create .env (copy env.example)
- php artisan key:generate
- Create and set database config in the env.
- Set SESSION_DOMAIN and SANCTUM_STATEFUL_DOMAINS based on your local environment domain.
- php artisan migrate.
- php artisan db:seed.
- npm run dev

## Usage

- The system have 2 types of user (admin, user).
- Refer to the database to identify which is admin(is_admin = 1).
- You can login as either user or admin. Both have different actions. Password is password.
- You can also use postman to access the api. Make a post request to login route to get the bearer token.
- Route can refer to the routes/api.php file.
- Uses basic VueJS SPA to demonstrate the api and Laravel Sanctum for authentication.

## Admin
 - View all user's loans.
 - Approve or reject user's loan applications.

 ## User
 - View user loans.
 - Apply new loans. User can select the amount, term (weeks) and currency based on the SOAP Public API.
 - Make payment to the approved loans. Payments need to be made based on the weekly loan term set by the user.

