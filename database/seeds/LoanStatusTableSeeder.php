<?php

use Illuminate\Database\Seeder;
use App\Models\LoanStatus;

class LoanStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LoanStatus::create([
            'status' => 'Pending'
        ]);

        LoanStatus::create([
            'status' => 'Approved'
        ]);

        LoanStatus::create([
            'status' => 'Rejected'
        ]);
    }
}
