<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = factory(App\User::class)->create();
        $admin->is_admin = 1;
        $admin->save();

        $user = factory(App\User::class)->create();
        $user->is_admin = 0;
        $user->save();
    }
}
