<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
// use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::all();

        return response()->json(new UserResource($users));
    }

    public function show()
    {
        if(Auth::user()) {
            return response()->json(new UserResource(Auth::user()));
        }

        return response()->json(['message' => 'User not found!'], 404);
    }
}
