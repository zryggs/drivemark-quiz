<?php

namespace App\Http\Controllers\API;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Http\Controllers\Controller;

class SoapController extends Controller
{
    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;

    /**
     * SoapController constructor.
     *
     * @param SoapWrapper $soapWrapper
     */
    public function __construct(SoapWrapper $soapWrapper)
    {
        $this->soapWrapper = $soapWrapper;
    }

    public function show()
    {
        $this->soapWrapper->add('Currency', function ($service) {
            $service
                ->wsdl('http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL')
                ->trace(true)
                ->options(['user_agent' => 'PHPSoapClient']);
        });

        $response = $this->soapWrapper->call('Currency.ListOfCurrenciesByName', []);

        $currencyObjects = $response->ListOfCurrenciesByNameResult->tCurrency;
        $currencies = [];

        foreach($currencyObjects as $currencyObject) {
            $newArray = [
                'isocode' => $currencyObject->sISOCode,
                'name' => $currencyObject->sISOCode . ' - ' . $currencyObject->sName
            ];

            array_push($currencies, $newArray);
        }

        return response()->json(['currencies' => $currencies], 200);
    }
}
