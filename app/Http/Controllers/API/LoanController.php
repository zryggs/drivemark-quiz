<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Loan;
use App\Models\LoanPayment;
use Auth;
use App\Http\Resources\LoanCollection;
use App\Http\Resources\LoanPaymentCollection;
use App\Http\Resources\LoanPaymentResource;
use App\Http\Resources\LoanResource;

class LoanController extends Controller
{
    public function index()
    {
        $loans = Auth::user()->loans;

        if($loans) {
            $response = [
                'message' => 'Loans found',
                'loans' => new LoanCollection($loans)
            ];
        } else {
            $response = [
                'message' => 'Loan not found',
            ];
        }

        return response()->json($response);
    }

    public function allUserLoans()
    {
        $loans = Loan::all();
        $all_loans = [];

        if($loans) {
            foreach($loans as $loan) {
                array_push($all_loans, new LoanResource($loan));
            }

            $response = [
                'message' => 'Loans found',
                'loans' => $all_loans
            ];
        } else {
            $response = [
                'message' => 'Loan not found',
            ];
        }

        return response()->json($response);
    }

    public function getAllLoanPayments()
    {
        $loans = Auth::user()->loans;

        if($loans) {
            $response = [
                'message' => 'Loans found',
                'loan_payments' => new LoanCollection($loans)
            ];
        } else {
            $response = [
                'message' => 'Loan not found',
            ];
        }

        return response()->json($response);
    }

    public function getLoanPayments(Request $request)
    {
        $loans = Loan::find($request->loan_id);

        if($loans) {
            $response = [
                'message' => 'Loan payments found',
                'loan_payments' => new LoanPaymentCollection($loans->loan_payments)
            ];
            $status = 200;
        } else {
            $response = [
                'message' => 'Loan payments not found',
            ];
            $status = 404;
        }

        return response()->json($response, $status);
    }

    public function applyLoan(Request $request) {
        $request->validate([
            'amount' => 'required|numeric|regex:/^\d+(\.\d{1,2})?$/',
            'term' => 'required|integer',
            'currency' => 'required|string'
        ]);

        $loan = Loan::create([
            'term' => $request->term,
            'currency' => $request->currency,
            'amount' => $request->amount,
            'user_id' => Auth::id(),
            'loan_status_id' => 1
        ]);

        for($counter = 1; $counter <= $request->term; $counter++) {
            LoanPayment::create([
                'loan_id' => $loan->id,
                'week' => $counter,
                'is_paid' => 0
            ]);
        }

        $response = [
            'message' => 'Loan apply success',
            'loan' => new LoanResource($loan)
        ];

        return response()->json($response);
    }

    public function payLoan(Request $request) {
        $request->validate([
            'loan_payment_id' => 'required|numeric',
        ]);

        $LoanPayment = LoanPayment::find($request->loan_payment_id);

        if($LoanPayment && $LoanPayment->loan->user_id == Auth::id() && $LoanPayment->loan->loan_status_id == 2) {
            $LoanPayment->update([
                'is_paid' => 1
            ]);

            return response()->json(['message' => 'Loan payment success', 'loan_payments' => new LoanPaymentResource($LoanPayment)], 200);
        } else {
            return response()->json(['message' => 'Loan payment failed'], 400);
        }
    }

    public function updateLoanStatus(Request $request) {
        $request->validate([
            'loan_id' => 'required|numeric',
            'status' => 'required|numeric|min:1|max:3'
        ]);

        $loan = Loan::find($request->loan_id);

        if($loan) {
            $loan->update([
                'loan_status_id' => $request->status
            ]);

            return response()->json(['message' => 'Loan status updated'], 200);
        } else {
            return response()->json(['message' => 'Loan status update failed'], 400);
        }
    }
}
