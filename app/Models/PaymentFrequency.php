<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentFrequency extends Model
{

    protected $table = 'payment_frequency';

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;

    protected $fillable = [
        'title',
        'type',
    ];

    public function loan()
    {
        return $this->hasOne('App\Models\Loan');
    }
}
