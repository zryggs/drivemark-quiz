<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{

    protected $table = 'loans';

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;

    protected $fillable = [
        'title',
        'amount',
        'currency',
        'term',
        'user_id',
        'loan_status_id',
        'payment_frequency_id'
    ];

    public function loan_payments()
    {
        return $this->hasMany('App\Models\LoanPayment');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function loan_status()
    {
        return $this->belongsTo('App\Models\LoanStatus');
    }

    public function payment_frequency()
    {
        return $this->belongsTo('App\Models\PaymentFrequency');
    }
}
