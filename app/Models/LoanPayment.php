<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanPayment extends Model
{

    protected $table = 'loan_payments';

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;

    protected $fillable = [
        'title',
        'loan_id',
        'week',
        'is_paid',
    ];

    public function loan()
    {
        return $this->belongsTo('App\Models\Loan');
    }
}
