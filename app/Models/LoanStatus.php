<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoanStatus extends Model
{

    protected $table = 'loan_statuses';

    // Primary Key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;

    protected $fillable = [
        'title',
        'status',
    ];

    public function loan()
    {
        return $this->hasOne('App\Models\Loan');
    }
}
