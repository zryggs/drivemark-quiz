require('./bootstrap');

import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import storeData from './store';

Vue.use(Vuex);
Vue.use(Vuetify);

import App from './components/App';

const store = new Vuex.Store(storeData);
const vuetify = new Vuetify();

// Vue.component('app', require('./components/App.vue').default);

store.dispatch('me').then(() => {
    new Vue({
        el: '#app',
        vuetify,
        components: { App },
        store
    });
});
