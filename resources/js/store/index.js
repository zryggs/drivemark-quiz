import axios from 'axios';

export default {
    namespaced: true,

    state: {
        authenticate: false,
        user: null
    },

    getters: {
        authenticated(state) {
            return state.authenticate;
        },

        user(state) {
            return state.user;
        }
    },

    mutations: {
        SET_AUTHENTICATED(state, value) {
            state.authenticate = value;
        },

        SET_USER(state, value) {
            state.user = value;
        }
    },

    actions: {
        async signIn({ dispatch }, credentials) {
            await axios.get('/sanctum/csrf-cookie');
            await axios.post('/login', credentials);

            return dispatch('me');
        },

        async signOut({ dispatch }) {
            await axios.post('/logout');

            return dispatch('me');
        },

        async me({ commit }) {
            return axios
                .get('/api/my-profile')
                .then((response) => {
                    commit('SET_AUTHENTICATED', true);
                    commit('SET_USER', response.data);
                })
                .catch(() => {
                    commit('SET_AUTHENTICATED', false);
                    commit('SET_USER', null);
                });
        }
    }
};
